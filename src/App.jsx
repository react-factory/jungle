function App() {
  // <> => encapsulation des composants
  return (
    <>
      <Header />
      <Description />
    </>
  );
}

function Header() {
  const title = "La maison jungle";
  return (
    <div>
      <h1> {title.toUpperCase()} </h1>
      <Cart />
    </div>
  );
}

function Cart() {
  const flowers = getFlowers();
  const flowerItems = flowers.map((flower) => (
    <li>
      {flower.name} : {flower.price} €
    </li>
  ));

  var total = 0;
  for (let i = 0; i < flowers.length; i++) {
    total += flowers[i].price;
  }

  return (
    <>
      <ul>{flowerItems}</ul>
      <div>Total du panier : {total} €</div>
    </>
  );
}

function Description() {
  const text = "Ici achetez toutes les plantes dont vous avez toujours rêvées";
  const emojis = "🤑🤑🤑";

  // slice -> subString
  return <p>{text.slice(0, 12) + emojis}</p>;
}

// export du functionnal component
export default App;

function getFlowers() {
  return [
    {
      name: "monstera",
      price: 8,
    },
    {
      name: "lierre",
      price: 10,
    },
    {
      name: "bouquet de fleurs",
      price: 15,
    },
  ];
}
